# 'region', 'area', 'locality', 'district', 'street', 'house'
es_mappings = {
    "Venue": {
        "properties": {
            "region": {
                "type": "string",
                "analyzer": "my_analyzer"
            },
            'locality': {
                "type": "string",
                "analyzer": "my_analyzer"
            },
            'district': {
                "type": "string",
                "analyzer": "my_analyzer"
            },
            'street': {
                "type": "string",
                "analyzer": "my_analyzer"
            },
            'house': {
                "type": "string",
                "analyzer": "my_analyzer"
            }
        }
    }
}

model_es_indices = {
    "Venue": {
        'index_name': "labdemo",
        "type": "Venue"
    }
}

fields_weights = {
    'locality': 5,
    'district': 4,
    'street': 3,
    'house': 2
}

fuzzy_fields_weights = {
    'locality': 1,
    'district': 1,
    'street': 1,
    'house': 1
}
