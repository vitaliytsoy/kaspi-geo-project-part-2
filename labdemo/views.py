from rest_framework.views import APIView
from labdemo.models import Venue
from labdemo.serializers import venueSerializer
from rest_framework.response import Response
from django.views.generic import TemplateView
from django.views.decorators.clickjacking import xframe_options_exempt
from django.utils.decorators import method_decorator

class SearchView(APIView):
    @method_decorator(xframe_options_exempt)
    def get(self, request):
        term = request.GET.get('text')
        addrs = Venue.es_search(term)
        venue_serializer = venueSerializer(addrs, many=True)
        response = {}
        response['addrs'] = venue_serializer.data
        return Response(response)


class IndexView(TemplateView):
    template_name = 'index.html'
