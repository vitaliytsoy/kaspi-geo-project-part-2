from rest_framework import serializers
from labdemo.models import Venue
from rest_framework_gis.serializers import GeoFeatureModelSerializer, GeometrySerializerMethodField
from django.contrib.gis.geos import Point


class venueSerializer(GeoFeatureModelSerializer):

    point = GeometrySerializerMethodField()

    def get_point(self, obj):
        return Point(obj.latitude, obj.longitude)

    class Meta:
        model = Venue
        geo_field = "point"
        fields = ('id', 'region', 'locality', 'district', 'house', 'longitude', 'latitude')
