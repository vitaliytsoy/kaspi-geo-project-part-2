from django.contrib.gis.db import models
from labdemo.es_mappings import model_es_indices, es_mappings
from django.conf import settings
import json

class Venue(models.Model):
    id = models.CharField(primary_key=True, max_length=350)
    region = models.CharField(max_length=750, blank=True, null=True)
    locality = models.CharField(max_length=750, blank=True, null=True)
    district = models.CharField(max_length=750, blank=True, null=True)
    street = models.CharField(max_length=750, blank=True, null=True)
    house = models.CharField(max_length=750, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    point = models.PointField(max_length=1000, blank=True, null=True)

    def es_repr(self):
        data = {}
        mapping = es_mappings[self.__class__.__name__]
        data['_id'] = self.pk
        for field_name in mapping['properties'].keys():
            data[field_name] = self.field_es_repr(field_name)
        return data

    def field_es_repr(self, field_name):
        mapping = es_mappings[self.__class__.__name__]
        config = mapping['properties'][field_name]

        if hasattr(self, 'get_es_%s' % field_name):
            field_es_value = getattr(self, 'get_es_%s' % field_name)()
        else:
            if config['type'] == 'object':
                related_object = getattr(self, field_name)
                field_es_value = {}
                field_es_value['_id'] = related_object.pk
                for prop in config['properties'].keys():
                    field_es_value[prop] = getattr(related_object, prop)
            else:
                field_es_value = getattr(self, field_name)

        return field_es_value

    @classmethod
    def get_es_index(cls):
        return model_es_indices[cls.__name__]['index_name']

    @classmethod
    def get_es_type(cls):
        return model_es_indices[cls.__name__]['type']

    @classmethod
    def es_search(cls, term, srch_fields=['region', 'locality', 'district', 'street', 'house']):
        es = settings.ES_CLIENT
        query = cls.gen_query(term, srch_fields)
        print json.dumps(query, ensure_ascii=False)
        recs = []
        res = es.search(index=cls.get_es_index(), doc_type=cls.get_es_type(), body=query)

        if res['hits']['total'] > 0:
            print 'found %s' % res['hits']['total']
            ids = [
                c['_id'] for c in res['hits']['hits']
                ]
            clauses = ' '.join(['WHEN id=\'%s\' THEN %s' % (pk, i) for i, pk in enumerate(ids)])
            ordering = 'CASE %s END' % clauses
            recs = cls.objects.filter(id__in=ids).extra(select={'ordering': ordering}, order_by=('ordering',))
            print recs[0]

        return recs

    @classmethod
    def gen_query(cls, term, srch_fields):
        val = term
        query = {
            "query": {
                "filtered": {
                    "query": {
                        "bool": {
                            "should": [
                                { "multi_match": {
                                    "type": "cross_fields",
                                    "fields": ["locality"],
                                    "fuzziness": "AUTO",
                                    "query": term,
                                    "boost": 10
                                } },
                                { "multi_match": {
                                    "type": "cross_fields",
                                    "fields": ["street", "district"],
                                    "fuzziness": "AUTO",
                                    "query": term,
                                    "boost": 5
                                } },
                                { "multi_match": {
                                    "type": "cross_fields",
                                    "fields": ["house"],
                                    "query": term
                                } }
                            ]
                        }
                    }
                }
            },
            "size": 10,
        }
        #
        # query['query']['bool']['should'] = [
        #     {"match": {
        #         field: {
        #             "query": val,
        #             "boost": fields_weights[field]
        #         }
        #     }} for field in fields_weights if field in srch_fields
        #     ]
        # query['query']['bool']['should'].extend([
        #                                             {"match": {
        #                                                 field: {
        #                                                     "query": val,
        #                                                     "boost": fuzzy_fields_weights[field],
        #                                                     "fuzziness": "AUTO"
        #
        #                                                 }
        #                                             }} for field in fuzzy_fields_weights if field in srch_fields
        #                                             ])
        #
        return json.dumps(query)

