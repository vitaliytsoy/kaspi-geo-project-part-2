var Geocoder = {
    init: function(map) {
        _this = this;
        _this._map = map;
        _this.bindElements();
        _this.bindEvents();
        _this.markerGroup = L.layerGroup().addTo(_this._map);
    },
    bindEvents: function(){
        // Инициализируем плагин typeahead
        _this.searchField.typeahead({
            minLength: 1,
            highlight: true
        },
        {
            limit: 20,
            displayKey: 'name',
            // При каждом изменении input поля отправляем запрос
            source: _this.sendRequest.bind(_this),
            templates: {
                suggestion: _this.renderTemplate.bind(_this),
                notFound: function () {
                    return '<div class="tt-no-result">Нет результатов...</div>';
                }
            }
        });
        // Инициализируем событие клика на результат поиска
        _this.searchField.bind('typeahead:select', _this.onResultItemClick.bind(_this));
    },
    bindElements: function () {
        _this.searchField = $('.js-search');
    },
    sendRequest: function (q, sync, async) {
        _this.markerGroup.clearLayers();
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8000/api/search/',
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            data: {
                'text': q
            },
            success: function(response){
                var data = new Array();
                var itemCollection = response.addrs.features;
                $.each(itemCollection, function(index, value){
                    var prop = value.properties
                    var name = prop.region + ' ' + prop.district + ' ' + prop.house;
                    data.push({
                        coordinates: value.geometry.coordinates,
                        name: name,
                        id: value.id
                    });
                });
                async(data);
            }
        });
        var icon = L.icon({
            iconUrl: '/static/imgs/icon.png'
        });
        var deficon = L.icon({
            iconUrl: '/static/imgs/deficon.png'
        });
        $(document).on('mouseover', '.list-item', function(){
            var id = $(this).attr('id');
            var allLayers = _this.markerGroup.getLayers();
            for(var i=0; i<allLayers.length; i++){
                if(allLayers[i]['options']['id'] === id){
                    allLayers[i].setIcon(icon);
                    allLayers[i].openPopup();
                }
            }
        }).on('mouseout', '.list-item', function(){
            var id = $(this).attr('id');
            var allLayers = _this.markerGroup.getLayers();
            for(var i=0; i<allLayers.length; i++){
                if(allLayers[i]['options']['id'] === id){
                    allLayers[i].setIcon(deficon);
                }
            }
        });
    },
    renderTemplate: function(data){
        var suggestion =    '<div class="list-item clearfix" id="' + data.id + '">' +
                                '<div class="list-item-r">' +
                                    '<h4 class="list-item-name item-name" title="' + data.name +
                                    '">' + data.name +  '</h4>' +
                                '</div>' +
                            '</div>';
        var deficon = L.icon({
            iconUrl: '/static/imgs/deficon.png'
        });
        var marker = L.marker(data.coordinates, {id: data.id}).bindPopup('<b>' + data.name + '</b>');
        marker.setIcon(deficon);
        marker.on('mouseover', function(){
            $(document).ready(function(){
                $('.tt-menu').css({
                    'display': 'block'
                });
                $('#' + marker.options.id).css({
                    'border': '1px solid red'
                });
            });
        }).on('mouseout', function(){
            $(document).ready(function(){
                $('#' + marker.options.id).css({
                    'border': 'none'
                });
            });
        });

        _this.markerGroup.addLayer(marker);
        return suggestion;
    },
    onResultItemClick: function(ev, suggestion){
        var coordinates = suggestion.coordinates;
        var deficon = L.icon({
            iconUrl: '/static/imgs/deficon.png'
        });
        var marker = L.marker(coordinates).bindPopup('<b>' + suggestion.name + '</b>');
        _this.clearMap();
        _this.markerGroup.addLayer(marker);
        _this._map.setView(new L.LatLng(coordinates[0], coordinates[1]), 15);
        marker.setIcon(deficon).openPopup()
    },
    clearMap: function () {
        _this.markerGroup.clearLayers();
    }
};