# -*- coding: utf-8 -*-
import requests
import json
import traceback
import time
import sys
import psycopg2
import threading
import urllib
import multiprocessing

version = '1.3'
key = 'ruczoy1743'
limit = 1000
processNumber = 0
data = []
counter = 0

def openJSON():
	global data
	with open('data.json') as json_data:
		data = json.load(json_data)

def calculateNumberOfProcesses():
	global processNumber
	total_addresses = len(data['addresses'])
	if int(total_addresses) % limit == 0:
		processNumber = (int(total_addresses)/limit)
	else:
		processNumber = (int(total_addresses)/limit) + 1

def getCoordinates(addresses):
	global version, key, counter
	for address in addresses:
		locality = address['locality']
		district = address['district']
		street = address['street']
		house = address['house']

		q = ''
		if locality != None:
			q = ' '.join([q, locality])
		if district != None:
			q = ' '.join([q, district])
		if street != None:
			q = ' '.join([q, street])
		if house != None:
			q = ' '.join([q, house])
		q = q.encode('utf-8')

		latitude = None
		longitude = None

		url = 'http://catalog.api.2gis.ru/geo/search'
		params = urllib.urlencode({'version': version,
								  'key': key,
								  'q': q})

		r = requests.get(url, params=params)
		if r:
			data = r.json()
			if 'result' in data:
				if 'selection' in data['result'][0]:
					selection = data['result'][0]['selection']
					if selection.find('POLYGON') == -1 and selection.find('MULTILINESTRING') == -1:
						selection = selection.replace('POINT(', '')
						selection = selection.replace(')', '')
						longitude = selection.split(' ', 1)[0]
						latitude = selection.split(' ', 1)[1]
						# write in database only if we can get address' location
						address['longitude'] = longitude
						address['latitude'] = latitude
						# print address
						writeToDB(address)
						counter += 1
						print counter

		# print for testing
		# print 'longitude = ', longitude
		# print "latitude = ", latitude
		# print '-------------------------'

def writeToDB(address):
	con = psycopg2.connect(host = 'localhost', database = 'kaspi_lab', user = 'compi', password = 'Newman20!', port = '5432')
	try:
		cursor = con.cursor()
		cursor.execute('INSERT INTO labdemo_venue (id, region, locality, district, street, house, latitude, longitude) ' + \
					 'VALUES (%s, %s, %s, %s, %s, %s, %s, %s)',
					 (address['rec_id'], address['region'], address['locality'], address['district'], address['street'], address['house'], address['latitude'], address['longitude']))
		con.commit()
	except:
		print(traceback.format_exc())
		con.rollback()
	finally:
		con.close()

def main():
	time_start = time.time()

	openJSON()
	calculateNumberOfProcesses()

	global processNumber, data, limit

	# TEST get locations for 10 objects from data.json
	# getCoordinates(data['addresses'][0:10])

	# get locations for all objects in data.json
	threadAddresses = []
	for i in range(processNumber):
		if i != processNumber-1:
			t = threading.Thread(target=getCoordinates, args=(data['addresses'][i*limit:(i+1)*limit],))
		else:
			t = threading.Thread(target=getCoordinates, args=(data['addresses'][i*limit:],))
		threadAddresses.append(t)
		t.start()
	for t in threadAddresses:
		t.join()

	time_end = time.time()
	print 'Overall processing is done in', time_end - time_start, 'sec'

	global counter
	print 'Find coordinates for', counter, 'addresses'

if __name__ == '__main__':
	main()
