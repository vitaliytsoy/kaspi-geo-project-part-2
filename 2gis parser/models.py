from django.contrib.gis import geos
from django.contrib.gis.db import models
from django.utils import timezone

class Venue(models.Model):
	venue_id = models.CharField(primary_key=True, max_length=350)
	region = models.CharField(max_length=750, blank=True, null=True)
	locality = models.CharField(max_length=750, blank=True, null=True)
	district = models.CharField(max_length=750, blank=True, null=True)
	street = models.CharField(max_length=750, blank=True, null=True)
	house = models.CharField(max_length=750, blank=True, null=True)
	latitude = models.FloatField(blank=True, null=True)
	longitude = models.FloatField(blank=True, null=True)
	point = models.PointField(max_length=1000, blank=True, null=True)
